package edu.towson.cosc431.Magarin.calculator


import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import kotlinx.android.synthetic.main.activity_main.*
import net.objecthunter.exp4j.ExpressionBuilder

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)



//Numbers
        tvZero.setOnClickListener { appendOnExpress("0", true) }
        tvDot.setOnClickListener { appendOnExpress(".", true) }
        tvClear.setOnClickListener { appendOnExpress("Clear", true) }
        tvDot.setOnClickListener { appendOnExpress(".", true) }
        tvSeven.setOnClickListener { appendOnExpress("7", true) }
        tvEight.setOnClickListener { appendOnExpress("8", true) }
        tvNine.setOnClickListener { appendOnExpress("9", true) }
        tvFour.setOnClickListener { appendOnExpress("4", true) }
        tvFive.setOnClickListener { appendOnExpress("5", true) }
        tvSix.setOnClickListener { appendOnExpress("6", true) }
        tvOne.setOnClickListener { appendOnExpress("1", true) }
        tvTwo.setOnClickListener { appendOnExpress("2", true) }
       tvThree.setOnClickListener { appendOnExpress("3", true) }


        //Symbols
        tvAdd.setOnClickListener { appendOnExpress("+", false) }
        tvSubtract.setOnClickListener { appendOnExpress("-", false) }
        tvMultiply.setOnClickListener { appendOnExpress("*", true) }
        tvDivide.setOnClickListener { appendOnExpress("/", true) }


        tvClear.setOnClickListener {
            tvExpression.text
            tvResult.text = ""
        }

        tvBack.setOnClickListener {
            val string = tvExpression.text.toString()
            if (string.isNotEmpty())

                tvExpression.text = string.substring(0, string.length - 1)
        }

    tvResult.text=""



        tvEquals.setOnClickListener{
            try{

                val expression = ExpressionBuilder(tvExpression.text.toString()).build()
                val result= expression.evaluate()
                val longResult=result.toLong()
                if(result== longResult.toDouble())
                    tvResult.text =longResult.toString()

                else
                    tvResult.text=longResult.toString()



            }catch (e:Exception){

                Log.d("Exception" ,  " message "+ e.message)
            }

        }



    }

fun appendOnExpress(string:String, canClear :  Boolean){



    if(canClear){
        tvResult.text= " "
        tvExpression.append(string) }

    else{
        tvExpression.append(tvResult.text)
        tvExpression.append(string)
        tvResult.text= " "
    }


}





}
